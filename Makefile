
CC = gcc
LD = $(CC)
CFLAGS = -Wall -g -pg -O0
CLIBS = -lm

src_root = ./src/
bin_root = ./bin/
lib_root = ./lib/
sim_root = ./sim/

SOURCE_MAIN = $(wildcard $(src_root)*.c)
OBJECTS_MAIN = $(patsubst %.c, %.o, $(SOURCE_MAIN))
POST_OBJECTS_MAIN = $(patsubst $(src_root)%, $(lib_root)%, $(OBJECTS_MAIN) )

SOURCE_EXE = $(wildcard $(sim_root)*.c)
EXECUTABLES = $(patsubst $(sim_root)%.c, $(bin_root)%.e, $(SOURCE_EXE))

.PHONY: default help  exec all clean 

default: help
	@echo $(POST_OBJECTS_MAIN)
	@echo $(EXECUTABLES)

help:
	@echo "Options:"
	@echo "make main:      compiler makes objects of the main files"
	@echo "make exec:      build executables from sim-src/"
	@echo "make clean:     delete output files"
	@echo "make help:      display this help"

main: $(POST_OBJECTS_MAIN)
exec: $(EXECUTABLES)

all: main exec

$(lib_root)%.o: $(src_root)%.c
	mkdir -p $(lib_root)
	$(CC) $(CFLAGS) -c $^ -o $@

$(bin_root)%.e: $(POST_OBJECTS_MAIN) $(sim_root)%.c
	mkdir -p $(bin_root)
	$(CC) $(CFLAGS) $(CLIBS) $^  -o $@

clean:
	rm -rfv $(POST_OBJECTS_MAIN) $(EXECUTABLES)

