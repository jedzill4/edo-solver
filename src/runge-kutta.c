#include "utils.h"
#include <stddef.h>
#include <stdlib.h>

void rk4(double *x, double *dxdt, int nvar, double t, double h, double *xout, void(*Derivs)(double,double*,double*)){
	/*
	 * nvar:		nvarumero de variables
	 * *x: 	Lista de variables {x0..xnvar}
	 * dxdt:	Lista de los valores de la derivada a tiempo t
	 * t:		tiempo
	 * h:		paso de tiempo
	 * xout:	Lista de salida (variables en el paso t+h)
	 * Derivs:	Funcion que retorna la derivada a tiempo t
	 */

	int i;
	double th,h2,h6,*k2,*k3,*xt;
	
	k2 = (double*)malloc((size_t) (sizeof(double))*(nvar+1));
	k3 = (double*)malloc((size_t) (sizeof(double))*(nvar+1));
	xt = (double*)malloc((size_t) (sizeof(double))*(nvar+1));
	if((!k2) || (!k3) || (!xt)) error("error en el alojamiento de memoria en rk4");

	h2=0.5*h;
	h6=h/6.0;
	th=t+h2;

	for(i=0;i<nvar;i++)	*(xt+i) = *(x+i) + *(dxdt+i)*h2; 	// dxdt = k1
	(*Derivs)(th,xt,k2);
	for(i=0;i<nvar;i++)	*(xt+i) = *(x+i) + *(k2+i)*h2;
	(*Derivs)(th,xt,k3);
	for(i=0;i<nvar;i++){
		*(xt+i) = *(x+i) + h*(*(k3+i));
		*(k2+i) += *(k3+i);					// k2 := k2+k3
	}
	(*Derivs)(t+h,xt,k3);						// k3 := k4
	for(i=0;i<nvar;i++) *(xout+i) = *(x+i) + h6*(*(dxdt+i) + *(k3+i) + 2.0*(*(k2+i)));

	free(k2);
	free(k3);
	free(xt);
}



//void rk22(double *x, double *dxdt, int nvar, double t, double h, double *xout, void(*Derivs)(double,double*,double*)){
	/*
	 * nvar:		nvarumero de variables
	 * *x: 	Lista de variables {x0..xnvar}
	 * dxdt:	Lista de los valores de la derivada a tiempo t
	 * t:		tiempo
	 * h:		paso de tiempo
	 * xout:	Lista de salida (variables en el paso t+h)
	 * Derivs:	Funcion que retorna la derivada a tiempo t
	 */
/*
	int n,i,nstep;
	double th,dh,h2,swap,*x1,*x2;

	nstep = 10;
	x1 = (double*)malloc((size_t) (sizeof(double))*(nvar+1));
	x2 = (double*)malloc((size_t) (sizeof(double))*(nvar+1));
	if((!x1) || (!x2)) error("error en el alojamiento de memoria en rk2");

	dh = h/nstep;
	for (i=0;i<nvar;i++){
		*(x1+i) = *(x+i);
		*(x2+i) = *(x+i)+*(dxdt+i)*dh;
	}
	th = t+dh;
	(*Derivs)(th,x2,xout);
	h2 = 0.5*dh;
	for(n=1;n<nstep;n++){
		for(i=0;i<nvar;i++){
			swap 	  = *(x1+i) + *(xout+i)*h2;
			*(x1+i) = *(x2+i);
			*(x2+i)  = swap;
		}	
		th += h;
		(*Derivs)(th,x2,xout);
	}
	for(i=0;i<nvar;i++) *(xout+i) = 0.5*(*(x1+i) + *(x2+i) + *(xout+i));

	free(x1);
	free(x2);
}
*/		
void rk2(void Derivs(double,double*,double*), double *h,  int nvar, double t, double dt){
	int i;
	double *k1,*k2,*xt;
	double dt2;

	k1 = (double*)malloc((size_t) (sizeof(double))*(nvar+1));
	k2 = (double*)malloc((size_t) (sizeof(double))*(nvar+1));
	xt = (double*)malloc((size_t) (sizeof(double))*(nvar+1));
	dt2 = 2.0*dt/3.0;

	for (i=0; i<nvar;i++){
		xt[i] = h[i];
	}

	Derivs(t,xt,k1);
	for(i=0;i<nvar;i++){
		xt[i] = h[i]+dt2*k1[i];
	}
	Derivs(t+dt2,xt,k2);


	for(i=0;i<nvar;i++)
//		h[i]=h[i] + dt *((k1[i]+3.0*k2[i])/4.0);
		*(h+i) = *(h+i) + dt*((*(k1+i) + *(k2+i)*3.)/4.);

	free(k1);
	free(k2);
	free(xt);
	return;
}







