#ifndef __RUNGE_KUTTA_H__
#define __RUNGE_KUTTA_H__


void rk4(double *x, double *dxdt, int N, double t, double h, double *xout, void(*Derivs)(double,double*,double*));

void rk2(void Derivs(double,double*,double*), double *h,  int N, double t, double dt);

#endif /* __RUNGE_KUTTA_H__ */
