#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "../src/runge-kutta.h"


#define 	N		3		// Population number
#define 	DT		1e-3	// Time step
#define     TMAX    10000    // Simulation time

double x[N];
double Dx[N];

const double b = 0.00001;
const double c = 0.0025;
const double d = 0.0025/7.;

void InitPopulation(){
    x[0] = 1000.;
    x[1] = 50.;
    x[2] = 0.;
}

void Deriv(double t, double* x, double* Dx){
    Dx[0] = -b*x[0]*x[1] + d*x[2];
    Dx[1] = +b*x[0]*x[1] - c*x[1];
    Dx[2] = +c*x[1] - d*x[2];
}

int main(){
	unsigned int i;
	double t=0.0;
    InitPopulation();

    while(t<TMAX){
        fprintf(stdout,"%.5f ",t);
        for(i=0;i<N;i++)
            fprintf(stdout,"%.5f ",x[i]);
        fprintf(stdout,"\n");
        
        rk4(x,Dx,N,t,DT,x,Deriv);
        t += DT;	
    }
	return 0;
}


