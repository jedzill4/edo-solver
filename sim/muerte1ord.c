#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "../src/runge-kutta.h"


#define 	N		1		// Population number
#define 	DT		1e-3	// Time step
#define     TMAX    1500    // Simulation time

double x[N];
double Dx[N];

const double dr = 1e-2;

void InitPopulation(){
    x[0] = 10000.;
}

void Deriv(double t, double* x, double* Dx){
    Dx[0] = -dr*x[0];
}

int main(){
	unsigned int i;
	double t=0.0;
    InitPopulation();

    while(t<TMAX){
        fprintf(stdout,"%.5f ",t);
        for(i=0;i<N;i++)
            fprintf(stdout,"%.5f ",x[i]);
        fprintf(stdout,"\n");
        
        rk4(x,Dx,N,t,DT,x,Deriv);
        t += DT;	
    }
	return 0;
}


